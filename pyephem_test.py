

from datetime import datetime, timedelta
from time import localtime, strftime
import ephem

home = ephem.Observer()
# replace lat, long, and elevation to yours
home.lat = '50.9'
home.long = '5.2'
home.elevation = 37

sun = ephem.Sun()

fmt = "%d-%b-%Y %H%MUTC"

if __name__ == '__main__':

    sun.compute(home)

    nextrise = home.next_rising(sun)
    nextset = home.next_setting(sun)

    print type(nextrise)

    print "next sunrise: ", ephem.localtime(nextrise)
    print "next sunset:  ", ephem.localtime(nextset)
