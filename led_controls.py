import threading
import logging
import time
import sys
import ledcontroller
#needs ledcontroller: pip install ledcontroller
from functools import partial
from datetime import datetime
import subprocess
import re

from PyQt5 import QtGui, uic, QtCore, QtWidgets
# from PyQt4.QtCore import QTimer, pyqtSlot

import os
import ephem
#pip install ephem

class LedControls(QtWidgets.QMainWindow):
    def __init__(self):
        super(LedControls, self).__init__()

        self.script_location = os.path.dirname(os.path.realpath(__file__))
        self.icon_location = os.path.join(self.script_location, "icons")
        uic.loadUi(os.path.join(self.script_location, 'led_controls.ui'), self)

        self.assign_button_icons()
        # # Set the window and tray icon to something


        icon = QtGui.QIcon(QtGui.QPixmap(os.path.join(self.icon_location, "power.png")))
        self.tray_icon = QtWidgets.QSystemTrayIcon()
        self.tray_icon.setIcon(QtGui.QIcon(icon))
        self.setWindowIcon(QtGui.QIcon(icon))

        # Restore the window when the tray icon is double clicked.
        self.tray_icon.activated.connect(self.restore_window)

        self.host = None
        self.dongle_macs = ["ac:cf:23:53:d5:fa"]

        self.led = ledcontroller.LedController(self.host, group_1 = "white")
        self.on = False
        self.fade_time = .75

        self.home = ephem.Observer()
        self.home.lat = "50.9"
        self.home.long = "5.2"
        self.home.elevation = 37
        self.sun = ephem.Sun()
        self.sunrise = None
        self.sunset = None

        self.btn_power.clicked.connect(self.power_on)
        self.btn_power_off.clicked.connect(self.power_off)
        self.btn_up.clicked.connect(partial(self.brightness_up, False))
        self.btn_down.clicked.connect(partial(self.brightness_down, False))
        self.btn_fade_up.clicked.connect(partial(self.fade_up, self.fade_time))
        self.btn_fade_down.clicked.connect(partial(self.fade_down, self.fade_time))
        self.btn_connect.clicked.connect(self.connect)


        self.connect()
        self.show()

    def find_ip_address(self):
        try:
            ip_addresses = []
            for line in subprocess.check_output(["arp"]).split("\n"):
                for mac_address in self.dongle_macs:
                    if mac_address in line:
                        ip_address = line[0:16].rstrip()
                        ip_addresses.append(ip_address)
    
                        logging.info( "Found dongle at: %s" % ip_address)

            if len(ip_addresses) > 0:
                return ip_addresses
            else:
                return None
        except:
            pass

    def connect(self):
        # ip_addresses = self.find_ip_address()
        # if ip_addresses == None:
        #     logging.info( "Can't find the dongle, using text input...")
        #     self.host = self.txt_host.text()
        # else:
        #     self.host = ip_addresses[0]
        #     for host in ip_addresses:
        #         self.cb_host.addItem(str(host))

        self.host = self.txt_host.text()
        logging.info("Using %s" % self.txt_host.text())
        self.led = ledcontroller.LedController(self.host, group_1 = "white")

    def assign_button_icons(self):
        all_buttons = self.findChildren(QtWidgets.QPushButton)
        for icon in [file for file in os.listdir(self.icon_location) if
                     os.path.isfile(os.path.join(self.icon_location, file))]:
            pixmap = QtGui.QPixmap(os.path.join(self.icon_location, icon))
            button_name = "btn_%s" % os.path.basename(icon)[:-4]
            for button in all_buttons:
                if button.objectName() == button_name:
                    button.setIcon(QtGui.QIcon(pixmap))

    def fade_up(self, sleep_time):
        for i in range(5):
            self.brightness_up()
            time.sleep(sleep_time)

    def fade_down(self, sleep_time):
        for i in range (5):
            self.brightness_down()
            time.sleep(sleep_time)

    def power_on(self):
        self.led.on()

    def power_off(self):
        self.led.off()

    def brightness_up_thread(self):
        self.led.brightness_up(1)

    def brightness_up(self, looping = False):
        up_thread = threading.Thread(target=self.brightness_up_thread)
        up_thread.setDaemon(True)
        up_thread.start()

    def brightness_down_thread(self):
        self.led.brightness_down(1)

    def brightness_down(self, looping = False):
        down_thread = threading.Thread(target=self.brightness_down_thread)
        down_thread.setDaemon(True)
        down_thread.start()

    def get_sunrise(self):
        self.sun.compute(self.home)
        sunrise = self.home.next_rising(self.sun)
        sunrise = ephem.localtime(sunrise)
        return sunrise

    def get_sunset(self):
        self.sun.compute(self.home)
        sunset = self.home.next_setting(self.sun)
        sunset = ephem.localtime(sunset)
        return sunset

    # def check_sunrise_sunset_thread(self):
    #     while True:
    #         print self.sunrise
    #         print self.sunset
    #         print datetime.now()
    #
    #         if self.sunrise is None and self.sunset is None:
    #             print "sunrise and sunset were None..."
    #             self.sunrise = self.get_sunrise()
    #             self.sunset = self.get_sunset()
    #             print "Getting sunrise and sunset times"
    #             print "Sunrise: %s" % self.sunrise
    #             print "Sunset: %s" % self.sunset
    #
    #         if self.sunrise.datetime() < datetime.now():
    #             self.sunrise = self.get_sunrise()
    #             print "Grabbed new sunrise time"
    #             print "Sunrise: %s" % self.sunrise
    #         if self.sunset.datetime() < datetime.now():
    #             self.sunset = self.get_sunset()
    #             print "Grabbed new sunset time"
    #             print "Sunset: %s" % self.sunset
    #
    #         time.sleep(14400)


    # def check_sunrise_sunset(self):
    #     sunrise_sunset_thread = threading.Thread(target=self.check_sunrise_sunset_thread)
    #     sunrise_sunset_thread.setDaemon(True)
    #     sunrise_sunset_thread.start()

    def reset(self):
        self.led.off()
        self.on == False



    def event(self, event):
        logging.info(event)
        if (event.type() == QtCore.QEvent.WindowStateChange and
                self.isMinimized()):
            # The window is already minimized at this point.  AFAIK,
            # there is no hook stop a minimize event. Instead,
            # removing the Qt.Tool flag should remove the window
            # from the taskbar.
            self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.Tool)
            self.tray_icon.show()
            return True
        else:
            return super(LedControls, self).event(event)

    def closeEvent(self, event):
        reply = QtWidgets.QMessageBox.question(
            self,
            'Message',"Quit the application? Clicking No minimizes to the system tray.",
            QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
            QtWidgets.QMessageBox.No)

        if reply == QtWidgets.QMessageBox.Yes:
            event.accept()
        else:
            self.tray_icon.show()
            self.hide()
            event.ignore()

    def restore_window(self, reason):
        logging.info("restoring")
        if reason == QtWidgets.QSystemTrayIcon.DoubleClick:
            self.tray_icon.hide()
            # self.showNormal will restore the window even if it was
            # minimized.
            self.showNormal()



if __name__ == "__main__":
    logging.info( "running app...")
    app = QtWidgets.QApplication([])
    client = LedControls()
    sys.exit(app.exec_())
